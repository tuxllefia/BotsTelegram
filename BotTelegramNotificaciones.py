# -*- coding: utf-8 -*-

# Este script sirve para enviar las notificaciones de las actuaciones diarias
# de mantenimiento. Esta limitado a un solo chat por el chatid, de esta forma
# solo yo recibire las notificaciones. Si borras el chat y lo abres de nuevo,
# por ejemplo si cambias de móvil, asegurate de cambiar el chatid.

import telebot
import sys
import os

# Como tengo hecho un script para hacer commit de los cambios en github si
# subo los archivos a pelo subire tanto el token como el chatid, de esta
# forma evito que se suban

# Este es nuestro token para el bot
archivoToken = open('/root/token.txt', 'r')
TOKEN = str(archivoToken.readline().rstrip('\n'))

# Chatid, la forma más fácil de obtenerlo es envian un mensaje al canal y entrando
# en esta página https://api.telegram.org/bot<TOKEN>/getUpdates?offset=0
archivoChatid = open('/root/id.txt', 'r')
chatid = str(archivoChatid.readline().rstrip('\n'))

# Cerramos los archivos
archivoToken.close()
archivoChatid.close()

# Preparamos nuestro bot. Todas las acciones se hacen con la clase "tb." lo que sea
tb = telebot.TeleBot(TOKEN)

# Los argumetos o parametros de ejecución haran que el bot nos avise de cada
# evento. Usamos el argumento 1 por que el argumento 0 es el nombre del archivo
# 1 -> para reinicios
# 2 -> para actualizaciones
# 3 -> para las copias de seguridad
# 4 -> para el borrado de las copias de seguridad

if sys.argv[1] == '1':
	tb.send_message(chatid,"Se ha reiniciado el equipo.")

if sys.argv[1] == '2':
	# Requiere de otro argumento adicional. Pasa el nombre del archivo donde está 
	# el registro de la actualización para poder enviarlo como adjunto.
	NombreArchivo =  sys.argv[2]
	tb.send_message(chatid,"Archivo de actualización de sistema...")
	
	# Lee el archivo en modo binario (rb)
	archivo = open(NombreArchivo, 'rb')
	tb.send_document(chatid, archivo)

	# Comprueba si se tiene que reiniciar algun servicio, requiere
	# la instalacion del paquete needrestart
	reinicioServicios = os.popen("needrestart -r a").read()
	tb.send_message(chatid,"Salida de needrestart -r a:\n\n"+reinicioServicios)

if sys.argv[1] == '3':
	# Requiere de otro dos argumentos adicionales donde nos envia el nombre del archivo y el
	# hash del mismo.
	Hash = sys.argv[2]
	NombreArchivo=sys.argv[3]
	tb.send_message(chatid,"Se ha creado la copia de seguridad en el siguiente archivo:\n"+NombreArchivo+"\n\nCon el hash SHA256: \n"+Hash)

if sys.argv[1] == '4':
	# Requiere de otros 2 argumentos adicionales con los nombres de los archivos borrados.
	NombreArchivo = sys.argv[2]
	NombreArchivoBBDD = sys.argv[3]
	tb.send_message(chatid,"Se ha eliminado el archivo de copia de seguridad "+NombreArchivo+"\n\nSe ha borrado el archivo de copia de seguridad de las bases de datos "+NombreArchivoBBDD)

if sys.argv[1] == '5':
	# Requiere de otro argumento con el nombre del archivo comprimido
	NombreArchivo = sys.argv[2]
	SumaSHA256 = sys.argv[3]
	tb.send_message(chatid,"Se ha generado el archivo con la copia de seguridad de las bases de datos: "+NombreArchivo+"\n\nCon el hash SHA256: "+SumaSHA256)

if sys.argv[1] == '6':
	# Avisa que no se ha borrado ningún archivo
	tb.send_message(chatid,"No se ha borrado ningun archivo de copia de seguridad. Solo quedan los últmos 3 días.")

if sys.argv[1] == '7':
	# Avisa del nuevo commit a github
	archivoDescripcion = open('descripcion.txt', 'r')
	textoCommit = str(archivoDescripcion.readline().rstrip('\n'))
	archivoDescripcion.close()

	tb.send_message(chatid,"Se ha publicado un nuevo commit en github:\n\n\""+textoCommit+"\"")

if sys.argv[1] == '8':
	# Este mensaje esta pensado para cuando pueda añadir clientes a la VPN desde el bot
	tb.send_message(chatid,"El usuario escogido para la red vpn ya existe.");

if sys.argv[1] == '9':
	# Notifica del nuevo archivo de backup del sistema y su correspondiente hash
	hash = sys.argv[2]
	tb.send_message(chatid,"Se ha creado una copia de seguridad del sistema en el archivo BackupSistema.tar.gz en el soporte para backups.\n\nCon el hash SHA256 \""+hash+"\"")

if len(sys.argv) <= 1:
	# Envia un mensaje si se ha ejecutado el script sin parametros
	tb.send_message(chatid,"Se ha ejecutado el script del bot sin parametros.")
